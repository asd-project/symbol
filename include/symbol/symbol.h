//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <string_view>

#include <meta/tuple/accumulate.h>
#include <meta/tuple/partition.h>
#include <meta/tuple/apply.h>
#include <meta/index.h>

#include <meta/types/contains.h>
#include <meta/types/count.h>
#include <meta/types/filter.h>
#include <meta/types/find.h>
#include <meta/types/nth.h>
#include <meta/types/remove_if.h>
#include <meta/types/transform.h>

//-----------------------------------------------------------------------------

namespace asd::symbol
{
    using namespace meta::literals;

    template <class... T>
    struct multi_validator
    { };

    template <class... A, class... T>
    struct multi_validator<multi_validator<A...>, T...> : multi_validator<A..., T...>
    {
        using multi_validator<A..., T...>::multi_validator;
    };

    template <class H, class... T>
    struct multi_validator<H, T...> : multi_validator<T...>
    {
        H v;

        constexpr multi_validator(H v, T... t)
            : multi_validator<T...> {t...}, v(v)
        {}

        template <class SymbolType, class SymbolTypes>
        constexpr auto operator()(SymbolType symbol_type, SymbolTypes provided_types) {
            return v(symbol_type, provided_types) && multi_validator<T...>::operator()(symbol_type, provided_types);
        }
    };

    template <>
    struct multi_validator<>
    {
        template <class SymbolType, class SymbolTypes>
        constexpr bool operator()(SymbolType symbol_type, SymbolTypes provided_types) {
            return true;
        }
    };

    template <class S, class T>
    struct named_value
    {
        using symbol_type = S;
        using value_type = T;

        named_value(T && value) noexcept
            : value(std::forward<T>(value))
        {}

        named_value(const named_value & v) noexcept
            : value(std::forward<T>(v.value))
        {}

        named_value & operator=(const named_value & v) noexcept = delete;

        T && value;
    };

    template <class T>
    struct unnamed_value
    {
        using value_type = T;

        unnamed_value(T && v) noexcept
            : v(std::forward<T>(v))
        {}

        unnamed_value(const unnamed_value & ref) noexcept
            : v(std::forward<T>(ref.v))
        {}

        unnamed_value & operator=(const unnamed_value & ref) = delete;

        constexpr decltype(auto) get() const noexcept {
            return std::forward<T>(v);
        }

        constexpr operator T &&() const noexcept {
            return std::forward<T>(v);
        }

        template <class... A>
        constexpr decltype(auto) operator()(A &&... args) const noexcept {
            return std::forward<T>(v)(std::forward<A>(args)...);
        }

        T && v;
    };

    template <class Symbol, class Validator>
    struct symbol_validator
    {
        using symbol_type = Symbol;
        using validator_type = Validator;

        Validator validator;

        template <class V>
        constexpr symbol_validator<Symbol, multi_validator<Validator, V>> operator[](V v) const {
            return {multi_validator<Validator, V>(validator, v)};
        }

        template <class SymbolTypes>
        constexpr auto operator()(SymbolTypes provided_types) {
            return validator(meta::type_v<Symbol>, provided_types);
        }
    };

    template <class T>
    struct symbol_type_of
    {
        using type = void;
    };

    template <class S, class V>
    struct symbol_type_of<named_value<S, V>>
    {
        using type = S;
    };

    template <class S, class V>
    struct symbol_type_of<symbol_validator<S, V>>
    {
        using type = S;
    };


    namespace detail
    {
        template <class>
        struct is_named_value : std::false_type {};

        template <class S, class V>
        struct is_named_value<named_value<S, V>> : std::true_type {};

        template <class>
        struct is_unnamed_value : std::false_type {};

        template <class T>
        struct is_unnamed_value<unnamed_value<T>> : std::true_type {};
    }

    template <class T>
    using is_named_value = symbol::detail::is_named_value<plain<T>>;

    template <class T>
    using is_unnamed_value = symbol::detail::is_unnamed_value<plain<T>>;

    template <bool B, class ... T>
    constexpr bool check() {
        return B;
    }

    template <class S>
    struct base
    {
        template <class T>
        constexpr named_value<S, T &&> operator = (T && value) const {
            return {std::forward<T>(value)};
        }

        template <class V>
        constexpr symbol_validator<S, V> operator[](V validator) const {
            return {validator};
        }
    };

    template <class ... T>
    class pack
    {
    public:
        using symbol_types = meta::types<typename symbol_type_of<plain<T>>::type...>;

        template <class ... A>
            requires(sizeof...(A) != 1 || !std::is_same_v<plain<meta::first_t<std::tuple<A...>>>, pack>)
        constexpr pack(A && ... args) : storage(std::forward<A>(args)...) {}

        constexpr pack(const pack &) = default;

        constexpr pack & operator = (const pack &) = default;

        template <class S>
        constexpr auto contains(symbol::base<S>) const noexcept {
            return meta::contains_v<symbol_types, S>;
        }

        template <class I, I v>
        constexpr decltype(auto) at(std::integral_constant<I, v> index) noexcept {
            return get<v>(storage);
        }

        template <class S>
        constexpr decltype(auto) at(symbol::base<S>) noexcept {
            constexpr auto index = meta::find_v<symbol_types, S>;

            if constexpr (index >= 0) {
                auto && named_value = get<index>(storage);

                using named_value_type = plaintype(named_value);
                using value_type = typename named_value_type::value_type;

                return static_cast<value_type &&>(named_value.value);
            } else {
                static_assert(check<(index >= 0), S, symbol_types>(), "Named value was not found in the given parameter pack");
            }
        }

        template <class S, class Default>
        constexpr decltype(auto) at(symbol::base<S>, Default && default_value) noexcept {
            constexpr auto index = meta::find_v<symbol_types, S>;

            if constexpr (index >= 0) {
                auto && named_value = get<index>(storage);

                using named_value_type = plaintype(named_value);
                using value_type = typename named_value_type::value_type;

                return static_cast<value_type &&>(named_value.value);
            } else if constexpr (std::invocable<Default>) {
                return default_value();
            } else {
                return std::forward<Default>(default_value);
            }
        }

        template <class I, I v>
        constexpr decltype(auto) operator[](std::integral_constant<I, v> index) noexcept {
            return at(index);
        }

        template <class S>
        constexpr decltype(auto) operator[](symbol::base<S> s) noexcept {
            return at(s);
        }

        std::tuple<T ...> storage;
    };
}

namespace std
{
    template <size_t Index, class ... T>
    struct tuple_element<Index, ::asd::symbol::pack<T...>> : std::tuple_element<Index, std::tuple<T...>> {};

    template <class ... T>
    struct tuple_size<::asd::symbol::pack<T...>> : std::integral_constant<size_t, sizeof...(T)> {};
}

namespace asd::symbol
{
    template <size_t Index, class ... T>
    constexpr decltype(auto) get(const symbol::pack<T...> & pack) {
        return get<Index>(pack.storage);
    }

    template <size_t Index, class ... T>
    constexpr decltype(auto) get(symbol::pack<T...> && pack) {
        return get<Index>(std::move(pack.storage));
    }

    template <class ... T>
    constexpr auto make_pack(T && ... args) {
        return pack<T...>(std::forward<T>(args)...);
    }

    template <class ... T, size_t... Indices>
    constexpr auto select(const symbol::pack<T...> & pack, std::index_sequence<Indices...>) {
        return symbol::make_pack(get<Indices>(pack)...);
    }

    template <class ... T, size_t... Indices>
    constexpr auto select(symbol::pack<T...> && pack, std::index_sequence<Indices...>) {
        return symbol::make_pack(get<Indices>(std::move(pack))...);
    }

    template <class T>
        requires (is_named_value<T>::value || is_unnamed_value<T>::value)
    constexpr decltype(auto) view(T && v) {
        return std::forward<T>(v);
    }

    template <class T>
        requires (!is_named_value<T>::value && !is_unnamed_value<T>::value)
    constexpr auto view(T && v) {
        return unnamed_value<T &&> {std::forward<T>(v)};
    }

    template <class... Values>
    constexpr auto collect(Values &&... v) {
        return meta::partition<is_named_value>(symbol::make_pack(symbol::view(std::forward<Values>(v))...));
    }

    template <class... A, class S>
    constexpr auto in(symbol::base<S>) {
        return meta::contains_v<meta::types<typename symbol_type_of<A>::type...>, S>;
    }

    template <class... A, class S>
    constexpr auto in(const symbol::pack<A...> &, symbol::base<S>) {
        return meta::contains_v<meta::types<typename symbol_type_of<A>::type...>, S>;
    }

    //---------------------------------------------------------------------------

    static constexpr auto required = [](auto symbol_type, auto provided_types) {
        using S = typename decltype(symbol_type)::type;

        constexpr auto is_present = meta::contains_v<decltype(provided_types), S>;
        static_assert(check<is_present, S, decltype(provided_types)>(), "Required named value is missing");

        return is_present;
    };

    static constexpr auto unique = [](auto symbol_type, auto provided_types) {
        using S = typename decltype(symbol_type)::type;

        constexpr auto is_unique = meta::count_v<S, decltype(provided_types)> <= 1;
        static_assert(check<is_unique, S, decltype(provided_types)>(), "Named value is passed multiple times");

        return is_unique;
    };

    static constexpr auto optional = [](auto...) {
        return true;
    };

    //---------------------------------------------------------------------------

    template <class SymbolType, class PossibleSymbols>
    constexpr auto check_expected(SymbolType symbol_type, PossibleSymbols possible_symbols) {
        using S = typename SymbolType::type;

        constexpr auto is_expected = meta::contains_v<decltype(possible_symbols), S>;
        static_assert(check<is_expected, S, decltype(possible_symbols)>(), "Given named value is not expected here");

        return is_expected;
    }

    template <class... Values, class... BoundValidators>
    constexpr auto matches(BoundValidators... v) {
        using types = meta::types<plain<Values>...>;
        using validators_types = meta::types<plain<BoundValidators>...>;

        using named_types = meta::filter_t<types, is_named_value>;

        using supported_symbol_types = meta::transform_t<validators_types, symbol_type_of>;
        using provided_symbol_types = meta::transform_t<named_types, symbol_type_of>;

        return meta::accumulate(provided_symbol_types{}, true, [=](bool acc, auto t) {
            return acc && check_expected(t, supported_symbol_types{});
        }) && (... && v(provided_symbol_types{}));
    }

    template <class F, class... BoundValues, class... Symbols>
    constexpr auto unpack(F f, std::tuple<BoundValues...> & v, Symbols... symbols) {
        return f((symbols = symbols(v))...);
    }

    template <class T, class... BoundValues, class... Symbols>
    constexpr T construct(std::tuple<BoundValues...> & v, Symbols... symbols) {
        return {(symbols = symbols(v))...};
    }

    namespace detail
    {
        template <class T>
        struct unwrap_type
        {
            using type = T;
        };

        template <class T>
        struct unwrap_type<meta::type<T>>
        {
            using type = T;
        };

        template <class T>
        struct unwrap_type<unnamed_value<T>>
        {
            using type = T;
        };
    }

    template <class T>
    using unwrapped_type = typename asd::symbol::detail::unwrap_type<plain<T>>::type;

    template <class Symbol, class ... A>
    using named_type = unwrapped_type<decltype(std::declval<symbol::pack<A...> &>().at(std::declval<Symbol>()))>;

    template <class Symbol, class Default, class ... A>
    using named_type_or_default = unwrapped_type<decltype(std::declval<symbol::pack<A...> &>().at(std::declval<Symbol>(), meta::type_v<Default>))>;

    template <class ... A>
    using unnamed_types = meta::remove_if_t<meta::types<A...>, is_named_value>;

    template <size_t Index, class ... A>
    using unnamed_type = unwrapped_type<typename meta::nth_t<Index, unnamed_types<A...>>::type>;

    template <class... A>
    constexpr size_t unnamed_count = std::tuple_size_v<unnamed_types<A...>>;
}

#define define_symbol(x)                               \
    namespace type                                     \
    {                                                  \
        struct x : public ::asd::symbol::base<x>       \
        {                                              \
            using ::asd::symbol::base<x>::operator =;  \
                                                       \
            static constexpr std::string_view name() { \
                return #x;                             \
            }                                          \
        };                                             \
    }                                                  \
                                                       \
    static constexpr type::x x {};
