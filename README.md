# Symbol

Inspired by [li::symbol](https://github.com/matt-42/lithium/tree/master/libraries/symbol) by Matthieu Garrigues.  

Symbols provide zero-overhead support for named function arguments and easy compile-time customization of function behavior depending on supplied set of arguments.  

Examples:

-   Passing named and unnamed arguments:

    ```cpp
    using namespace ui::s;

    ui::scroll_area(
        controller = ui::scroll_controller,
        
        ui::container(...),
        ...
    )
    ``` 

-   Symbols declaration:

    ```cpp
    namespace ui {
        namespace s {
            define_symbol(controller)
        }
    ...
        
    ```

-   Extraction of passed values and types:

    ```cpp
    template <class ... T>
    auto scroll_area(T &&... options) {
        auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);
        
        using content_type = symbol::unnamed_type<0, T...>;
        using widget_type = widgets::scroll_area<content_type>;

        widget_type area(std::move(unnamed[0_c]));

        auto && v = s::controller(named);
        area.set_controller(v(area));
        ...
    ```

    Parameters with associated names are passed through temporary objects of type `symbol::named_value<S, V>` to encode compile-time symbol information. Unnamed arguments are passed by reference and wrapped into objects of type `symbol::unnamed_value<V>` when grouped together in the `symbol::collect` function to save reference type information. These wrappers disappear completely in runtime after optimizations (tested on Clang 11 with "-O2"). 
    
    Named values are accessed by applying `operator()` of the required symbol to a pack of named values. Unnamed values are accessed using a compile-time index (`""_c` is user-defined literal, which returns a value of type `std::integral_constant<size_t, 0>`).  
    You can use `symbol::named_type<type::{symbol_name}>` to access a value type associated with symbol.  
    `symbol::named_type` automatically unwraps `meta::type`, so you can write `symbol = meta::type_v<T>` to pass types:

    ```cpp
    ui::container(
        element_type = meta::type_v<ui::panel>,
        layout = ui::grid(7, 4.0f, 4.0f),
        ...
    ```

    <p align="center"> ↓ </p>

    ```cpp
    using element_type = symbol::named_type<s::type::element_type, T...>;
    using layout_type = symbol::named_type<s::type::layout, T...>;

    container_type<element_type, layout_type> container(...);
    ...
    ```

-   Validation of provided bound values with readable error messages in a function interface:

    ```cpp
    template <class ... T> requires (
        symbol::matches<T...> (
            s::controller   [symbol::optional][symbol::unique]
        )
    )
    auto func(T &&... options) ...
    ```

-   Constexpr checks of optional values:

    ```cpp
    if constexpr (symbol::in<T...>(s::controller)) {
        auto && v = s::controller(named);
        area.set_controller(v(area));
        ...
    ```

## Differences from li::symbol
Implementation differs from `li::symbol` in several aspects to avoid visual noise and provide more granular control over symbols usage in library implementations:
- There are no symbol generation utilities and macro guards - symbols should be defined in domain namespaces so there may be symbols with the same name in different namespaces. They may be selectively pulled in a call site via `using` or `using namespace`.  
- The symbol declaration macro doesn't automatically wrap symbols into a namespace.  
- Both named and unnamed arguments are supported to improve interface usability.  
- Parameters validation is performed via concepts to prevent further compilation of a function body when incorrect arguments are supplied. Unsupported arguments that are not specified in a validation expression are __NOT__ ignored and cause a compilation error.  
